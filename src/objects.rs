use crate::KeyPairAlgorithm;
use pkcs11::types::*;

#[derive(Debug)]
pub struct PrivateKey {
    id: Vec<u8>,
    label: String,
    private: bool,
    token: bool,
    extractable: bool,
    sign: bool,
    sensitive: bool,
    unwrap: bool,
    algorithm: KeyPairAlgorithm,
}

impl PrivateKey {
    pub fn from_attributes(attributes: Vec<CK_ATTRIBUTE>) -> PrivateKey {
        let id = attributes[0].get_bytes().unwrap();
        let label = attributes[1].get_string().unwrap();
        let private = attributes[2].get_bool().unwrap();
        let token = attributes[3].get_bool().unwrap();
        let extractable = attributes[4].get_bool().unwrap();
        let sign = attributes[5].get_bool().unwrap();
        let sensitive = attributes[6].get_bool().unwrap();
        let unwrap = attributes[7].get_bool().unwrap();
        let key_type = attributes[8].get_ck_ulong().unwrap();

        let algorithm = match_key_pair_algorithm(key_type);

        PrivateKey {
            id,
            label,
            private,
            token,
            extractable,
            sign,
            sensitive,
            unwrap,
            algorithm,
        }
    }

    pub fn get_algorithm(&self) -> KeyPairAlgorithm {
        self.algorithm
    }
}

#[derive(Debug)]
pub struct PublicKey {
    id: Vec<u8>,
    label: String,
    token: bool,
    algorithm: KeyPairAlgorithm,
}

impl PublicKey {
    pub fn from_attributes(attributes: Vec<CK_ATTRIBUTE>) -> PublicKey {
        let id = attributes[0].get_bytes().unwrap();
        let label = attributes[1].get_string().unwrap();
        let token = attributes[2].get_bool().unwrap();
        let key_type = attributes[3].get_ck_ulong().unwrap();

        let algorithm = match_key_pair_algorithm(key_type);

        PublicKey {
            id,
            label,
            token,
            algorithm,
        }
    }

    pub fn get_algorithm(&self) -> KeyPairAlgorithm {
        self.algorithm
    }
}

#[derive(Debug)]
pub struct Certificate {
    pub id: Vec<u8>,
    pub label: String,
    pub private: bool,
    pub token: bool,
    pub r#type: CertificateType,
    pub category: CertificateCategory,
    pub value: Vec<u8>,
    // pub start_date: Instant,
    // pub end_date: Instant,
    pub subject: Vec<u8>,
    pub issuer: Vec<u8>,
    pub serial_number: Vec<u8>,
    // subject_public_key_hash: Vec<u8>,
    // issuer_public_key_hash: Vec<u8>,
    // hash_algorithm: HashAlgorithm,
    // modifiable: bool,
    // copyable: bool,
    // trusted: bool,
}

impl Certificate {
    pub fn from_attributes(attributes: Vec<CK_ATTRIBUTE>) -> Certificate {
        let id = attributes[0].get_bytes().unwrap();
        let value = attributes[1].get_bytes().unwrap();
        let certificate_type = attributes[2].get_ck_ulong().unwrap();
        let certificate_category = attributes[3].get_ck_ulong().unwrap();
        let label = attributes[4].get_string().unwrap();
        let private = attributes[5].get_bool().unwrap();
        let token = attributes[6].get_bool().unwrap();
        let subject = attributes[7].get_bytes().unwrap();
        let issuer = attributes[8].get_bytes().unwrap();
        let serial_number = attributes[9].get_bytes().unwrap();

        let r#type = match certificate_type {
            CKC_X_509 => CertificateType::PublicKeyCertificate,
            CKC_X_509_ATTR_CERT => CertificateType::AttributesCertificate,
            _ => panic!("Unknown certificate type: {}", certificate_type),
        };

        let category = match certificate_category {
            CK_CERTIFICATE_CATEGORY_UNSPECIFIED => CertificateCategory::Unspecified,
            CK_CERTIFICATE_CATEGORY_TOKEN_USER => CertificateCategory::TokenUser,
            CK_CERTIFICATE_CATEGORY_AUTHORITY => CertificateCategory::Authority,
            CK_CERTIFICATE_CATEGORY_OTHER_ENTITY => CertificateCategory::OtherEntity,
            _ => panic!("Unknown certificate category: {}", certificate_category),
        };

        Certificate {
            id,
            label,
            private,
            token,
            r#type,
            category,
            value,
            subject,
            issuer,
            serial_number,
            // start_date,
            // end_date,
        }
    }
}

#[derive(Debug)]
pub enum CertificateType {
    PublicKeyCertificate,
    AttributesCertificate,
}

#[derive(Debug)]
pub enum CertificateCategory {
    Unspecified,
    TokenUser,
    Authority,
    OtherEntity,
}

fn match_key_pair_algorithm(key_type: CK_ULONG) -> KeyPairAlgorithm {
    match key_type {
        CKK_EC => KeyPairAlgorithm::EC,
        CKK_RSA => KeyPairAlgorithm::RSA,
        CKK_GOSTR3410 => KeyPairAlgorithm::GOSTR3410,
        _ => panic!("Unsupported key type: {:?}", key_type),
    }
}
