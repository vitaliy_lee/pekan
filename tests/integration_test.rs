use std::mem;
use std::sync::{Arc, Mutex, Once};

use fcrypto::CSP;

use hex;

#[cfg(target_os = "linux")]
const LIBRARY: &str = "/usr/lib/softhsm/libsofthsm2.so";

#[cfg(target_os = "macos")]
const LIBRARY: &str = "/usr/local/lib/softhsm/libsofthsm2.so";

const LABEL: &str = "pekan_test";
const PIN: &str = "0000";

#[derive(Clone)]
struct SingletonReader {
    // Since we will be used in many threads, we need to protect
    // concurrent access
    inner: Arc<Mutex<pekan::CSP>>,
}

fn singleton() -> SingletonReader {
    // Initialize it to a null value
    static mut SINGLETON: *const SingletonReader = 0 as *const SingletonReader;
    static ONCE: Once = Once::new();

    unsafe {
        ONCE.call_once(|| {
            // Make it
            let csp = init_csp();

            let reader = SingletonReader {
                inner: Arc::new(Mutex::new(csp)),
            };

            // Put it in the heap so it can outlive this call
            let reader_ref = Box::new(reader);

            SINGLETON = mem::transmute(reader_ref);
        });

        // Now we give out a copy of the data that is safe to use concurrently.
        (*SINGLETON).clone()
    }
}

fn init_csp() -> pekan::CSP {
    let options = pekan::Options {
        library: LIBRARY.to_string(),
        label: LABEL.to_string(),
        pin: PIN.to_string(),
        session_cache_size: None,
    };

    pekan::CSP::init(options).unwrap()
}

#[test]
fn test_sign_verify() {
    let reader = singleton();
    let csp = reader.inner.lock().unwrap();

    let message = "test_message".as_bytes();
    let ski =
        hex::decode("e586b6e411722d4f22abe83f55b67e057750968d53a1883087777548389aba27").unwrap();

    let signer_result = csp.new_signer(&ski, fcrypto::DigestAlgorithm::SHA256);
    assert!(signer_result.is_ok());

    let signer = signer_result.unwrap();

    let signature_result = signer.sign(message);
    assert!(signature_result.is_ok());

    let signature = signature_result.unwrap();
    assert!(!signature.is_empty());

    let verifier_result = csp.new_verifier(&ski, fcrypto::DigestAlgorithm::SHA256);
    assert!(verifier_result.is_ok());

    let verifier = verifier_result.unwrap();

    let verifier_result = verifier.verify(&signature, message);
    assert!(
        verifier_result.is_ok(),
        "Failed to verify signature: {:?}",
        verifier_result.err()
    );

    let valid = verifier_result.unwrap();
    assert!(valid);

    // println!("signature: {}", hex::encode(&signature));

    // let r = &signature[..signature.len()/2];
    // let s = &signature[signature.len()/2..];

    // println!("R: {}, S: {}", hex::encode(r), hex::encode(s));

    // R = new(big.Int)
    // S = new(big.Int)
    // R.SetBytes(sig[0 : len(sig)/2])
    // S.SetBytes(sig[len(sig)/2:])
}

#[test]
fn test_generate_random() {
    const LENGTH: u64 = 24;

    let reader = singleton();
    let csp = reader.inner.lock().unwrap();

    let result = csp.generate_random(LENGTH);

    assert_eq!(result.is_ok(), true);

    let random_data = result.unwrap();

    assert_eq!(random_data.len() as u64, LENGTH);

    println!("random_data: {:x?}", random_data);
}

#[test]
fn test_digest() {
    const TEST_MESSAGE: &str = "test_message";
    const EXPECTED_DIGEST: &str =
        "3b7491dc016ac1a0b2e02372402c861fafa459294087e7cbe09f704d582d931f";

    let reader = singleton();
    let csp = reader.inner.lock().unwrap();

    let message = TEST_MESSAGE.as_bytes();

    let result = csp.digest(message, fcrypto::DigestAlgorithm::SHA256);

    assert_eq!(result.is_ok(), true);
    let digest = result.unwrap();

    let expected = hex::decode(EXPECTED_DIGEST).unwrap();
    assert_eq!(expected, digest);
}

#[test]
fn test_generate_keypair() {
    let reader = singleton();
    let csp = reader.inner.lock().unwrap();

    let result = csp.generate_key_pair(pekan::KeyPairAlgorithm::EC, true);

    println!("{:?}", result);

    assert!(result.is_ok());

    let (pub_key_handle, priv_key_handle) = result.unwrap();

    println!(
        "pub_key_handle: {:?} priv_key_handle: {:?}",
        pub_key_handle, priv_key_handle
    );
}
