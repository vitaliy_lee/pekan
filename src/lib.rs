#![allow(clippy::needless_return)]

use std::convert::TryInto;
use std::error;
use std::ptr;
use std::sync::{Arc, RwLock};

use pkcs11::errors::Error;
use pkcs11::types::*;
use pkcs11::Ctx;

use fcrypto::Digester;

// const DEFAULT_SESSION_POOL_SIZE: usize = 3;

pub mod config;
pub mod errors;
pub mod objects;

#[derive(Debug, Default)]
pub struct Options {
    pub library: String,
    pub label: String,
    pub pin: String,
    pub session_cache_size: Option<usize>,
    // pub flags: u32,
}

#[derive(Debug, Copy, Clone)]
pub enum KeyPairAlgorithm {
    RSA,
    EC,
    GOSTR3410,
}

#[derive(Debug, Clone, Copy)]
pub enum KeyType {
    SecretKey,
    PrivateKey,
    PublicKey,
}

impl KeyType {
    fn object_class(self) -> CK_OBJECT_CLASS {
        match self {
            Self::SecretKey => CKO_SECRET_KEY,
            Self::PrivateKey => CKO_PRIVATE_KEY,
            Self::PublicKey => CKO_PUBLIC_KEY,
        }
    }
}

pub struct CSP {
    options: Options,
    context: Arc<RwLock<Ctx>>,
    slot: CK_SLOT_ID,
    session: CK_SESSION_HANDLE,
}

impl CSP {
    pub fn init(options: Options) -> Result<CSP, Error> {
        let pin_code = Some(options.pin.as_str());

        let context = Ctx::new_and_initialize(&options.library).unwrap();

        let slot = find_slot(&context, &options)?;

        let session =
            context.open_session(slot, CKF_SERIAL_SESSION | CKF_RW_SESSION, None, None)?;
        context.login(session, CKU_USER, pin_code)?;

        // let mut context = Ctx::new(&options.library).unwrap();
        // let init_args = CK_C_INITIALIZE_ARGS::new();
        // context.initialize(Some(init_args)).unwrap();

        let csp = CSP {
            options,
            context: Arc::new(RwLock::new(context)),
            slot,
            session,
        };

        return Ok(csp);
    }

    pub fn finalize(&mut self) -> Result<(), Error> {
        let mut context = self.context.write().unwrap();
        context.finalize()
    }

    pub fn generate_key_pair(
        &self,
        algorithm: KeyPairAlgorithm,
        ephemeral: bool,
    ) -> Result<(CK_OBJECT_HANDLE, CK_OBJECT_HANDLE), Error> {
        match algorithm {
            KeyPairAlgorithm::EC => self.generate_ec_keypair(ephemeral),
            _ => panic!("Unsupported key pair algorithm"),
            // KeyPairAlgorithm::RSA => CKK_RSA,
            // KeyPairAlgorithm::GOSTR3410 => CKK_GOSTR3410,
            // KeyPairAlgorithm::UNKNOWN => panic!("Unknown key pair algorithm"),
        }
    }

    fn generate_ec_keypair(
        &self,
        ephemeral: bool,
    ) -> Result<(CK_OBJECT_HANDLE, CK_OBJECT_HANDLE), Error> {
        let token = if ephemeral { CK_FALSE } else { CK_TRUE };

        let pub_key_label = String::from("ca-hsm-pub");

        let params: Vec<u8> = vec![6, 8, 42, 134, 72, 206, 61, 3, 1, 7];
        // log::debug!("params: {:x?}", params);

        let pub_key_template = vec![
            // CK_ATTRIBUTE::new(CKA_ID).with_bytes(&ski),
            CK_ATTRIBUTE::new(CKA_LABEL).with_string(&pub_key_label),
            CK_ATTRIBUTE::new(CKA_KEY_TYPE).with_ck_ulong(&CKK_EC),
            CK_ATTRIBUTE::new(CKA_CLASS).with_ck_ulong(&CKO_PUBLIC_KEY),
            CK_ATTRIBUTE::new(CKA_TOKEN).with_bool(&token),
            CK_ATTRIBUTE::new(CKA_VERIFY).with_bool(&CK_TRUE),
            CK_ATTRIBUTE::new(CKA_EC_PARAMS).with_bytes(&params),
        ];

        let priv_key_label = String::from("ca-hsm-priv");

        let priv_key_template = vec![
            // CK_ATTRIBUTE::new(CKA_ID).with_bytes(&ski),
            CK_ATTRIBUTE::new(CKA_LABEL).with_string(&priv_key_label),
            CK_ATTRIBUTE::new(CKA_KEY_TYPE).with_ck_ulong(&CKK_EC),
            CK_ATTRIBUTE::new(CKA_CLASS).with_ck_ulong(&CKO_PRIVATE_KEY),
            CK_ATTRIBUTE::new(CKA_TOKEN).with_bool(&token),
            CK_ATTRIBUTE::new(CKA_PRIVATE).with_bool(&CK_TRUE),
            CK_ATTRIBUTE::new(CKA_SIGN).with_bool(&CK_TRUE),
            CK_ATTRIBUTE::new(CKA_SENSITIVE).with_bool(&CK_TRUE),
            CK_ATTRIBUTE::new(CKA_EXTRACTABLE).with_bool(&CK_FALSE),
        ];

        let mechanism = new_keypair_mechanism(KeyPairAlgorithm::EC);

        let context = self.context.read().unwrap();

        let (pub_key_handle, priv_key_handle) = context.generate_key_pair(
            self.session,
            &mechanism,
            &pub_key_template,
            &priv_key_template,
        )?;

        Ok((pub_key_handle, priv_key_handle))
    }

    fn new_signer(
        &self,
        digest_algorithm: fcrypto::DigestAlgorithm,
        algorithm: KeyPairAlgorithm,
        priv_key_handle: CK_OBJECT_HANDLE,
    ) -> Result<Box<dyn fcrypto::Signer>, Error> {
        let context = self.context.read().unwrap();

        let session = open_session(&context, self.slot, &self.options)?;

        let digest_mechanism = new_digest_mechanism(digest_algorithm);
        let sign_mechanism = new_sign_mechanism(algorithm);

        let signer = PKCS11Signer::new(
            self.context.clone(),
            digest_mechanism,
            sign_mechanism,
            session,
            priv_key_handle,
        );

        return Ok(Box::new(signer));
    }

    fn new_verifier(
        &self,
        digest_algorithm: fcrypto::DigestAlgorithm,
        algorithm: KeyPairAlgorithm,
        pub_key_handle: CK_OBJECT_HANDLE,
    ) -> Result<Box<dyn fcrypto::Verifier>, Error> {
        let context = self.context.read().unwrap();

        let session = open_session(&context, self.slot, &self.options)?;

        let digest_mechanism = new_digest_mechanism(digest_algorithm);
        let sign_mechanism = new_sign_mechanism(algorithm);

        let verifier = PKCS11Verifier::new(
            self.context.clone(),
            digest_mechanism,
            sign_mechanism,
            session,
            pub_key_handle,
        );

        return Ok(Box::new(verifier));
    }

    pub fn find_key_by_label(
        &self,
        label: &str,
        key_type: KeyType,
    ) -> Result<CK_OBJECT_HANDLE, Box<dyn error::Error>> {
        let context = self.context.read().unwrap();

        let session = open_session(&context, self.slot, &self.options)?;

        let class = key_type.object_class();

        let template = vec![
            CK_ATTRIBUTE::new(CKA_LABEL).with_bytes(label.as_bytes()),
            CK_ATTRIBUTE::new(CKA_CLASS).with_ck_ulong(&class),
        ];

        context.find_objects_init(session, &template)?;

        let objects = context.find_objects(session, 1)?;
        context.find_objects_final(session)?;

        log::debug!("Found keys: {:?}", objects);

        if objects.is_empty() {
            let error = errors::Error {
                reason: "Key not found".to_string(),
            };

            return Err(Box::new(error));
        }

        return Ok(objects[0]);
    }

    fn find_key_by_ski(
        &self,
        ski: &[u8],
        key_type: KeyType,
    ) -> Result<CK_OBJECT_HANDLE, Box<dyn error::Error>> {
        let context = self.context.read().unwrap();

        let session = open_session(&context, self.slot, &self.options)?;

        let class = key_type.object_class();

        let template = vec![
            CK_ATTRIBUTE::new(CKA_ID).with_bytes(ski),
            CK_ATTRIBUTE::new(CKA_CLASS).with_ck_ulong(&class),
        ];

        context.find_objects_init(session, &template)?;

        let objects = context.find_objects(session, 1)?;
        context.find_objects_final(session)?;

        log::debug!("Found keys: {:?}", objects);

        if objects.is_empty() {
            let error = errors::Error {
                reason: "Key not found".to_string(),
            };

            return Err(Box::new(error));
        }

        return Ok(objects[0]);
    }

    fn find_private_keys(&self) -> Result<Vec<CK_OBJECT_HANDLE>, Box<dyn error::Error>> {
        let context = self.context.read().unwrap();

        let session = open_session(&context, self.slot, &self.options)?;

        let template = vec![CK_ATTRIBUTE::new(CKA_CLASS).with_ck_ulong(&CKO_PRIVATE_KEY)];

        context.find_objects_init(session, &template)?;

        let objects = context.find_objects(session, 1)?;
        context.find_objects_final(session)?;

        return Ok(objects);
    }

    pub fn get_private_key(
        &self,
        key_handle: CK_OBJECT_HANDLE,
    ) -> Result<objects::PrivateKey, Box<dyn error::Error>> {
        let mut template = vec![
            CK_ATTRIBUTE::new(CKA_ID),
            CK_ATTRIBUTE::new(CKA_LABEL),
            CK_ATTRIBUTE::new(CKA_PRIVATE),
            CK_ATTRIBUTE::new(CKA_TOKEN),
            CK_ATTRIBUTE::new(CKA_EXTRACTABLE),
            CK_ATTRIBUTE::new(CKA_SIGN),
            CK_ATTRIBUTE::new(CKA_SENSITIVE),
            CK_ATTRIBUTE::new(CKA_UNWRAP),
            CK_ATTRIBUTE::new(CKA_KEY_TYPE),
        ];

        let context = self.context.read().unwrap();

        let session = open_session(&context, self.slot, &self.options)?;

        {
            // (CK_RV, &'a Vec<CK_ATTRIBUTE>)
            let (rv, _) = context.get_attribute_value(session, key_handle, &mut template)?;
            log::debug!("CK_RV: 0x{:?}, Template: {:?}", rv, &template);
        }

        let id: Vec<CK_BYTE> = Vec::with_capacity(template[0].ulValueLen.try_into().unwrap());
        template[0].set_bytes(id.as_slice());

        let label = String::with_capacity(template[1].ulValueLen.try_into().unwrap());
        template[1].set_string(&label);

        let private: CK_BBOOL = 0;
        template[2].set_bool(&private);

        let token: CK_BBOOL = 0;
        template[3].set_bool(&token);

        let extractable: CK_BBOOL = 0;
        template[4].set_bool(&extractable);

        let sign: CK_BBOOL = 0;
        template[5].set_bool(&sign);

        let sensitive: CK_BBOOL = 0;
        template[6].set_bool(&sensitive);

        let unwrap: CK_BBOOL = 0;
        template[7].set_bool(&unwrap);

        let key_type: CK_ULONG = 0;
        template[8].set_ck_ulong(&key_type);

        let (rv, _attributes) = context.get_attribute_value(session, key_handle, &mut template)?;
        log::debug!("CK_RV: 0x{:x}, Retrieved Attributes: {:?}", rv, template);

        let private_key = objects::PrivateKey::from_attributes(template);
        Ok(private_key)
    }

    fn get_public_key(
        &self,
        key_handle: CK_OBJECT_HANDLE,
    ) -> Result<objects::PublicKey, Box<dyn error::Error>> {
        let mut template = vec![
            CK_ATTRIBUTE::new(CKA_ID),
            CK_ATTRIBUTE::new(CKA_LABEL),
            CK_ATTRIBUTE::new(CKA_TOKEN),
            CK_ATTRIBUTE::new(CKA_KEY_TYPE),
        ];

        let context = self.context.read().unwrap();

        let session = open_session(&context, self.slot, &self.options)?;

        {
            // (CK_RV, &'a Vec<CK_ATTRIBUTE>)
            let (rv, _) = context.get_attribute_value(session, key_handle, &mut template)?;
            log::debug!("CK_RV: 0x{:?}, Template: {:?}", rv, &template);
        }

        let id: Vec<CK_BYTE> = Vec::with_capacity(template[0].ulValueLen.try_into().unwrap());
        template[0].set_bytes(id.as_slice());

        let label = String::with_capacity(template[1].ulValueLen.try_into().unwrap());
        template[1].set_string(&label);

        let token: CK_BBOOL = 0;
        template[2].set_bool(&token);

        let key_type: CK_ULONG = 0;
        template[3].set_ck_ulong(&key_type);

        let (rv, _attributes) = context.get_attribute_value(session, key_handle, &mut template)?;
        log::debug!("CK_RV: 0x{:x}, Retrieved Attributes: {:?}", rv, template);

        let public_key = objects::PublicKey::from_attributes(template);
        Ok(public_key)
    }

    pub fn find_certificate_by_label(
        &self,
        label: &str,
    ) -> Result<CK_OBJECT_HANDLE, Box<dyn error::Error>> {
        let context = self.context.read().unwrap();

        let session = open_session(&context, self.slot, &self.options)?;

        let class: CK_ULONG = CKO_CERTIFICATE;

        let template = vec![
            CK_ATTRIBUTE::new(CKA_CLASS).with_ck_ulong(&class),
            CK_ATTRIBUTE::new(CKA_LABEL).with_bytes(label.as_bytes()),
        ];

        context.find_objects_init(session, &template)?;

        let objects = context.find_objects(session, 1)?;
        context.find_objects_final(session)?;

        log::debug!("Found certificates: {:?}", objects);

        if objects.is_empty() {
            let error = errors::Error {
                reason: "Certificate not found".to_string(),
            };

            return Err(Box::new(error));
        }

        return Ok(objects[0]);
    }

    pub fn import_certificate(
        &self,
        certificate: &objects::Certificate,
    ) -> Result<CK_OBJECT_HANDLE, Error> {
        log::debug!("Importing {:?}", certificate);

        let context = self.context.read().unwrap();

        let session = open_session(&context, self.slot, &self.options)?;

        let class = CKO_CERTIFICATE;

        let token = if certificate.token { CK_TRUE } else { CK_FALSE };

        let private = if certificate.private {
            CK_TRUE
        } else {
            CK_FALSE
        };

        let certificate_type = match &certificate.r#type {
            objects::CertificateType::PublicKeyCertificate => CKC_X_509,
            objects::CertificateType::AttributesCertificate => CKC_X_509_ATTR_CERT,
        };

        let certificate_category = match certificate.category {
            objects::CertificateCategory::Unspecified => CK_CERTIFICATE_CATEGORY_UNSPECIFIED,
            objects::CertificateCategory::TokenUser => CK_CERTIFICATE_CATEGORY_TOKEN_USER,
            objects::CertificateCategory::Authority => CK_CERTIFICATE_CATEGORY_AUTHORITY,
            objects::CertificateCategory::OtherEntity => CK_CERTIFICATE_CATEGORY_OTHER_ENTITY,
        };

        let template = vec![
            CK_ATTRIBUTE::new(CKA_ID).with_bytes(&certificate.id),
            CK_ATTRIBUTE::new(CKA_CLASS).with_ck_ulong(&class),
            CK_ATTRIBUTE::new(CKA_VALUE).with_bytes(&certificate.value),
            CK_ATTRIBUTE::new(CKA_CERTIFICATE_TYPE).with_ck_ulong(&certificate_type),
            CK_ATTRIBUTE::new(CKA_CERTIFICATE_CATEGORY).with_ck_ulong(&certificate_category),
            // common attributes
            CK_ATTRIBUTE::new(CKA_LABEL).with_string(&certificate.label),
            // CK_ATTRIBUTE::new(CKA_MODIFIABLE).with_bool(modifiable),
            // CK_ATTRIBUTE::new(CKA_COPYABLE).with_bool(&certificate.copyable),
            CK_ATTRIBUTE::new(CKA_TOKEN).with_bool(&token),
            CK_ATTRIBUTE::new(CKA_PRIVATE).with_bool(&private),
            // CKA_START_DATE
            // CKA_END_DATE
            CK_ATTRIBUTE::new(CKA_SUBJECT).with_bytes(&certificate.subject),
            CK_ATTRIBUTE::new(CKA_ISSUER).with_bytes(&certificate.issuer),
            CK_ATTRIBUTE::new(CKA_SERIAL_NUMBER).with_bytes(&certificate.serial_number),
            // CKA_TRUSTED
            // CKA_CHECK_VALUE
            // CKA_HASH_OF_SUBJECT_PUBLIC_KEY
            // CKA_HASH_OF_ISSUER_PUBLIC_KEY
            // CKA_NAME_HASH_ALGORITHM
        ];

        context.create_object(session, &template)
    }

    pub fn get_certificate(
        &self,
        key_handle: CK_OBJECT_HANDLE,
    ) -> Result<objects::Certificate, Box<dyn error::Error>> {
        let mut template = vec![
            CK_ATTRIBUTE::new(CKA_ID),
            CK_ATTRIBUTE::new(CKA_VALUE),
            CK_ATTRIBUTE::new(CKA_CERTIFICATE_TYPE),
            CK_ATTRIBUTE::new(CKA_CERTIFICATE_CATEGORY),
            // common attributes
            CK_ATTRIBUTE::new(CKA_LABEL),
            // CK_ATTRIBUTE::new(CKA_MODIFIABLE).with_bool(modifiable),
            // CK_ATTRIBUTE::new(CKA_COPYABLE).with_bool(&certificate.copyable),
            CK_ATTRIBUTE::new(CKA_PRIVATE),
            CK_ATTRIBUTE::new(CKA_TOKEN),
            // CKA_START_DATE
            // CKA_END_DATE
            CK_ATTRIBUTE::new(CKA_SUBJECT),
            CK_ATTRIBUTE::new(CKA_ISSUER),
            CK_ATTRIBUTE::new(CKA_SERIAL_NUMBER),
            // CKA_TRUSTED
            // CKA_CHECK_VALUE
            // CKA_HASH_OF_SUBJECT_PUBLIC_KEY
            // CKA_HASH_OF_ISSUER_PUBLIC_KEY
            // CKA_NAME_HASH_ALGORITHM
        ];

        let context = self.context.read().unwrap();

        let session = open_session(&context, self.slot, &self.options)?;

        {
            // (CK_RV, &'a Vec<CK_ATTRIBUTE>)
            let (rv, _) = context.get_attribute_value(session, key_handle, &mut template)?;
            log::debug!("CK_RV: 0x{:?}, Template: {:?}", rv, &template);
        }

        let id: Vec<CK_BYTE> = Vec::with_capacity(template[0].ulValueLen.try_into().unwrap());
        template[0].set_bytes(id.as_slice());

        let value: Vec<CK_BYTE> = Vec::with_capacity(template[1].ulValueLen.try_into().unwrap());
        template[1].set_bytes(value.as_slice());

        let certificate_type: CK_ULONG = 0;
        template[2].set_ck_ulong(&certificate_type);

        let certificate_category: CK_ULONG = 0;
        template[3].set_ck_ulong(&certificate_category);

        let label = String::with_capacity(template[4].ulValueLen.try_into().unwrap());
        template[4].set_string(&label);

        let private: CK_BBOOL = 0;
        template[5].set_bool(&private);

        let token: CK_BBOOL = 0;
        template[6].set_bool(&token);

        let subject: Vec<CK_BYTE> = Vec::with_capacity(template[7].ulValueLen.try_into().unwrap());
        template[7].set_bytes(subject.as_slice());

        let issuer: Vec<CK_BYTE> = Vec::with_capacity(template[8].ulValueLen.try_into().unwrap());
        template[8].set_bytes(issuer.as_slice());

        let serial_number: Vec<CK_BYTE> =
            Vec::with_capacity(template[9].ulValueLen.try_into().unwrap());
        template[9].set_bytes(serial_number.as_slice());

        let (rv, _attributes) = context.get_attribute_value(session, key_handle, &mut template)?;
        log::debug!("CK_RV: 0x{:x}, Retrieved Attributes: {:?}", rv, template);

        let certificate = objects::Certificate::from_attributes(template);

        Ok(certificate)
    }

    fn import_private_key(_private_key: &objects::PrivateKey) -> Result<CK_OBJECT_HANDLE, Error> {
        // parse_ec_private_key();
        // parse_rsa_private_key();

        // create_private_key();

        // let template = vec![
        //     CK_ATTRIBUTE::new(CKA_EC_POINT).with_bytes(&certificate.id),
        //     CK_ATTRIBUTE::new(CKA_EC_PARAMS).with_bytes(&class),
        // ];

        unimplemented!();
    }
}

impl Drop for CSP {
    fn drop(&mut self) {
        if let Err(error) = self.finalize() {
            log::error!("Failed to finalize library: {}", error);
        }
    }
}

impl fcrypto::CSP for CSP {
    fn digest(
        &self,
        message: &[u8],
        algorithm: fcrypto::DigestAlgorithm,
    ) -> fcrypto::Result<Vec<u8>> {
        let mechanism = new_digest_mechanism(algorithm);

        let context = self.context.read().unwrap();

        context.digest_init(self.session, &mechanism)?;
        context.digest(self.session, message).map_err(convert_error)
    }

    fn get_digester(
        &self,
        algorithm: fcrypto::DigestAlgorithm,
    ) -> fcrypto::Result<Box<dyn fcrypto::Digester>> {
        let context = self.context.read().unwrap();

        let mechanism = new_digest_mechanism(algorithm);
        let session = open_session(&context, self.slot, &self.options)?;
        let mut digester = PKCS11Digester::new(self.context.clone(), mechanism, session);
        digester.init()?;
        return Ok(Box::new(digester));
    }

    fn generate_random(&self, length: u64) -> fcrypto::Result<Vec<u8>> {
        let context = self.context.read().unwrap();

        context
            .generate_random(self.session, length)
            .map_err(convert_error)
    }

    fn new_signer(
        &self,
        ski: &[u8],
        algorithm: fcrypto::DigestAlgorithm,
    ) -> fcrypto::Result<Box<dyn fcrypto::Signer>> {
        let key_handle = self.find_key_by_ski(ski, KeyType::PrivateKey)?;

        let private_key = self.get_private_key(key_handle)?;

        self.new_signer(algorithm, private_key.get_algorithm(), key_handle)
            .map_err(convert_error)
    }

    fn new_verifier(
        &self,
        ski: &[u8],
        algorithm: fcrypto::DigestAlgorithm,
    ) -> fcrypto::Result<Box<dyn fcrypto::Verifier>> {
        let key_handle = self.find_key_by_ski(ski, KeyType::PublicKey)?;

        let public_key = self.get_public_key(key_handle)?;

        self.new_verifier(algorithm, public_key.get_algorithm(), key_handle)
            .map_err(convert_error)
    }
}

fn open_session(
    context: &Ctx,
    slot: CK_SLOT_ID,
    options: &Options,
) -> Result<CK_SESSION_HANDLE, Error> {
    context.open_session(slot, CKF_SERIAL_SESSION | CKF_RW_SESSION, None, None)
}

#[inline(always)]
fn new_mechanism(mechanism_type: u64) -> CK_MECHANISM {
    let mechanism = CK_MECHANISM {
        mechanism: mechanism_type,
        pParameter: ptr::null_mut(),
        ulParameterLen: 0,
    };

    return mechanism;
}

#[inline(always)]
fn new_keypair_mechanism(algorithm: KeyPairAlgorithm) -> CK_MECHANISM {
    let mechanism_type = match algorithm {
        KeyPairAlgorithm::RSA => CKM_RSA_PKCS_KEY_PAIR_GEN,
        KeyPairAlgorithm::EC => CKM_EC_KEY_PAIR_GEN,
        KeyPairAlgorithm::GOSTR3410 => CKM_GOSTR3410_KEY_PAIR_GEN,
    };

    return new_mechanism(mechanism_type);
}

#[inline(always)]
fn new_digest_mechanism(algorithm: fcrypto::DigestAlgorithm) -> CK_MECHANISM {
    let mechanism_type = match algorithm {
        fcrypto::DigestAlgorithm::SHA256 => CKM_SHA256,
        fcrypto::DigestAlgorithm::SHA512 => CKM_SHA512,
    };

    return new_mechanism(mechanism_type);
}

#[inline(always)]
fn new_sign_mechanism(algorithm: KeyPairAlgorithm) -> CK_MECHANISM {
    let mechanism_type = match algorithm {
        KeyPairAlgorithm::EC => CKM_ECDSA,
        KeyPairAlgorithm::RSA => CKM_RSA_PKCS,
        KeyPairAlgorithm::GOSTR3410 => CKM_GOSTR3410,
    };

    return new_mechanism(mechanism_type);
}

fn find_slot(context: &Ctx, options: &Options) -> Result<CK_SLOT_ID, Error> {
    let slots = context.get_slot_list(true)?;

    for slot in slots {
        let token_info = context.get_token_info(slot)?;

        let label = String::from(token_info.label);

        if label == options.label {
            return Ok(slot);
        }
    }

    return Err(Error::Module("token not found"));
}

pub struct PKCS11Signer {
    digest_mechanism: CK_MECHANISM,
    sign_mechanism: CK_MECHANISM,
    context: Arc<RwLock<Ctx>>,
    session: CK_SESSION_HANDLE,
    priv_key_handle: CK_OBJECT_HANDLE,
}

impl PKCS11Signer {
    fn new(
        context: Arc<RwLock<Ctx>>,
        digest_mechanism: CK_MECHANISM,
        sign_mechanism: CK_MECHANISM,
        session: CK_SESSION_HANDLE,
        priv_key_handle: CK_OBJECT_HANDLE,
    ) -> PKCS11Signer {
        let signer = PKCS11Signer {
            digest_mechanism,
            sign_mechanism,
            context,
            session,
            priv_key_handle,
        };

        return signer;
    }
}

impl fcrypto::Signer for PKCS11Signer {
    fn sign(&self, message: &[u8]) -> fcrypto::Result<Vec<u8>> {
        log::debug!("Signing message: {:x?}", message);

        let context = self.context.read().unwrap();

        context.digest_init(self.session, &self.digest_mechanism)?;
        let digest = context.digest(self.session, message)?;

        context.sign_init(self.session, &self.sign_mechanism, self.priv_key_handle)?;
        let signature = context.sign(self.session, &digest)?;

        return Ok(signature);
    }
}

struct PKCS11Digester {
    mechanism: CK_MECHANISM,
    context: Arc<RwLock<Ctx>>,
    session: CK_SESSION_HANDLE,
}

impl PKCS11Digester {
    fn new(
        context: Arc<RwLock<Ctx>>,
        mechanism: CK_MECHANISM,
        session: CK_SESSION_HANDLE,
    ) -> PKCS11Digester {
        let digester = PKCS11Digester {
            mechanism,
            context,
            session,
        };

        return digester;
    }
}

impl fcrypto::Digester for PKCS11Digester {
    fn init(&mut self) -> fcrypto::Result<()> {
        let context = self.context.read().unwrap();

        context
            .digest_init(self.session, &self.mechanism)
            .map_err(convert_error)
    }

    fn update(&mut self, data: &[u8]) -> fcrypto::Result<()> {
        let context = self.context.read().unwrap();

        context
            .digest_update(self.session, data)
            .map_err(convert_error)
    }

    fn digest(&mut self) -> fcrypto::Result<Vec<u8>> {
        let context = self.context.read().unwrap();

        context.digest_final(self.session).map_err(convert_error)
    }
}

fn convert_error(error: Error) -> Box<dyn std::error::Error> {
    let reason = format!("{}", error);
    Box::new(errors::Error { reason })
}

pub struct PKCS11Verifier {
    context: Arc<RwLock<Ctx>>,
    session: CK_SESSION_HANDLE,
    pub_key_handle: CK_OBJECT_HANDLE,
    digest_mechanism: CK_MECHANISM,
    sign_mechanism: CK_MECHANISM,
}

impl PKCS11Verifier {
    fn new(
        context: Arc<RwLock<Ctx>>,
        digest_mechanism: CK_MECHANISM,
        sign_mechanism: CK_MECHANISM,
        session: CK_SESSION_HANDLE,
        pub_key_handle: CK_OBJECT_HANDLE,
    ) -> PKCS11Verifier {
        let signer = PKCS11Verifier {
            digest_mechanism,
            sign_mechanism,
            context,
            session,
            pub_key_handle,
        };

        return signer;
    }
}

impl fcrypto::Verifier for PKCS11Verifier {
    fn verify(&self, signature: &[u8], message: &[u8]) -> fcrypto::Result<bool> {
        log::debug!("Verifying message: {:x?}", message);

        let context = self.context.read().unwrap();

        context.digest_init(self.session, &self.digest_mechanism)?;
        let digest = context.digest(self.session, message)?;

        context.verify_init(self.session, &self.sign_mechanism, self.pub_key_handle)?;

        match context.verify(self.session, &digest, signature) {
            Ok(()) => Ok(true),
            Err(error) => Err(Box::new(error)),
        }
    }
}
