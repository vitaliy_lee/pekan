use criterion::{criterion_group, criterion_main, Criterion};

use fcrypto::CSP;

#[cfg(target_os = "linux")]
const LIBRARY: &str = "/usr/lib/softhsm/libsofthsm2.so";

#[cfg(target_os = "macos")]
const LIBRARY: &str = "/usr/local/lib/softhsm/libsofthsm2.so";

fn criterion_benchmark(c: &mut Criterion) {
    let options = pekan::Options {
        library: LIBRARY.to_string(),
        label: "pekan_test".to_string(),
        pin: "0000".to_string(),
        session_cache_size: None,
    };

    let csp = pekan::CSP::init(options).expect("init CSP");

    let message = "Fabrik!Fabrik!".as_bytes();

    c.bench_function("digest SHA256", |b| {
        b.iter(|| {
            let _digest = csp
                .digest(message, fcrypto::DigestAlgorithm::SHA256)
                .unwrap();
        })
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
