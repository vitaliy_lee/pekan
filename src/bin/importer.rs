use std::env;
use std::fs;
use std::path::Path;
use std::process;

// use std::time;

use pekan::objects::{Certificate, CertificateCategory, CertificateType};
use pekan::{Options, CSP};

use openssl::x509::X509;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 4 {
        println!("usage: importer <config_path> <certificate_path> <certificate_label>");
        process::exit(1);
    }

    let config_path = Path::new(&args[1]);

    let config = pekan::config::AppConfig::read(config_path).expect("parse config");

    let options = Options {
        library: config.library,
        label: config.label,
        pin: config.pin,
        session_cache_size: Some(3),
    };

    let certificate_file_path = &args[2];
    let certificate_label = &args[3];

    let csp = CSP::init(options).expect("init CSP");

    let data = fs::read(certificate_file_path).expect("read certificate file");

    let x509 = X509::from_pem(&data).expect("parse certificate from pem");

    let value = x509.to_der().expect("encode certificate to DER");

    // let subject = x509.subject_name().to_der()
    // 	.expect("encode subject to DER");
    // let issuer = x509.issuer_name().to_der()
    // 	.expect("encode issuer to DER");

    // let serial_number = x509.serial_number().to_der()
    // 	.expect("encode issuer to DER");

    let serial_number = Default::default();
    let subject = Default::default();
    let issuer = Default::default();

    // TODO:
    // let start_date: time::Instant = x509.not_before().into();
    // let end_date: time::Instant = x509.not_after().into();

    let certificate = Certificate {
        id: vec![0xC, 0xA, 0xF, 0xE],
        label: certificate_label.to_owned(),
        private: false,
        token: true,
        r#type: CertificateType::PublicKeyCertificate,
        category: CertificateCategory::Authority,
        value,
        subject,
        issuer,
        serial_number,
        // start_date,
        // end_date,
    };

    match csp.import_certificate(&certificate) {
        Ok(_handle) => println!("Certificate imported"),
        Err(error) => println!("Failed to import certificate: {}", error),
    }
}
