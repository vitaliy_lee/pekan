use std::error::Error;
use std::fs;
use std::io::prelude::*;
use std::path::Path;

use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
pub struct AppConfig {
    pub library: String,
    pub label: String,
    pub pin: String,
}

impl AppConfig {
    pub fn read<P: AsRef<Path>>(path: P) -> Result<AppConfig, Box<dyn Error>> {
        let path = path.as_ref();

        let mut config_file = fs::File::open(path)?;

        let mut content = String::new();
        config_file.read_to_string(&mut content)?;

        let app_confg: AppConfig = toml::from_str(&content)?;

        Ok(app_confg)
    }
}
