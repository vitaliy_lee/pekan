use std::env;
use std::path::Path;
use std::process;

use pekan::{KeyType, Options, CSP};

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 4 {
        println!("usage: tokken <config_path> <private_key_label> <certificate_label>");
        process::exit(1);
    }

    let config_path = Path::new(&args[1]);

    let config = pekan::config::AppConfig::read(config_path).expect("parse config");

    let options = Options {
        library: config.library,
        label: config.label,
        pin: config.pin,
        session_cache_size: Some(3),
    };

    let csp = CSP::init(options).expect("init CSP");

    let private_key_label = &args[2];

    let private_key_handle = csp
        .find_key_by_label(private_key_label, KeyType::PrivateKey)
        .expect("find private keys");

    let private_key = csp
        .get_private_key(private_key_handle)
        .expect("get private key");

    println!("{:?}", private_key);

    let certificate_label = &args[3];

    let certificate_handle = csp
        .find_certificate_by_label(certificate_label)
        .expect("find certificate");

    let certificate = csp
        .get_certificate(certificate_handle)
        .expect("get certificate");

    println!("{:?}", certificate);
}
